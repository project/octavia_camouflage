document.addEventListener('DOMContentLoaded', () => {
  // Functions to open and close a modal
  function openModal($el) {
    $el.classList.add('is-active');
  }

  function closeModal($el) {
    $el.classList.remove('is-active');
  }

  function closeAllModals() {
    (document.querySelectorAll('.modal') || []).forEach(($modal) => {
      closeModal($modal);
    });
  }

  // Add a click event on buttons to open a specific modal
  (document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
    const modal = $trigger.dataset.target;
    const $target = document.getElementById(modal);

    $trigger.addEventListener('click', () => {
      openModal($target);
    });
  });

  // Add a click event on various child elements to close the parent modal
  (document.querySelectorAll('.modal-background, .modal-close, .modal-content .delete, .modal-card-foot .button') || []).forEach(($close) => {
    const $target = $close.closest('.modal');

    $close.addEventListener('click', () => {
      closeModal($target);
    });
  });

  // Add a keyboard event to close all modals
  document.addEventListener('keydown', (event) => {
    const e = event || window.event;

    if (e.keyCode === 27) { // Escape key
      closeAllModals();
    }
  });
});

window.addEventListener("load", (event) => {
  const addEventIcon = document.querySelector(
    ".app-menu__item.menu-item--expanded"
  );
  if (addEventIcon !== null) {
    const addEventUl = addEventIcon.querySelector("ul");
    addEventIcon.addEventListener("click", function () {
      addEventUl.classList.toggle("active");
    });
  }

  // Dark mode
  const body = document.querySelector("body");
  let darkModeStorage = window.localStorage.getItem("darkMode");

  if (darkModeStorage === null) {
    // if storage is not set, match the browser preference
    if (
      window.matchMedia &&
      window.matchMedia("(prefers-color-scheme: dark)").matches
    ) {
      if (!body.classList.contains("dark")) {
        body.classList.add("dark");
      }
    }
  } else {
    // since it's defined, let's set it to what's in storage
    if (darkModeStorage === "true") {
      if (!body.classList.contains("dark")) {
        body.classList.add("dark");
      }
    }
  }

  //the toggle
  const accountMenu = document.querySelector(
    "#block-useraccountmenu nav ul"
  );
  const darkModeToggle = document.createElement("li");
  darkModeToggle.classList.add("darkmode-toggle");
  accountMenu.append(darkModeToggle);
  darkModeToggle.addEventListener("click", function (e) {
    e.preventDefault();
    if (body.classList.contains("dark")) {
      body.classList.remove("dark");
      window.localStorage.setItem("darkMode", false);
    } else {
      body.classList.add("dark");
      window.localStorage.setItem("darkMode", true);
    }
  });


  // Accordions

  // Search form accordion
  const accordionForm =
    document.getElementById("views-exposed-form-event-events-listing-list") ||
    document.getElementById("views-exposed-form-event-events-listing-card")
  if (accordionForm) {
      const formToggle = document.querySelector(".form-accordion-toggle") || null;
      if (new URL(window.location.href).search !== "" && formToggle) {
        formToggle.classList.remove("is-hidden");
        accordionForm.classList.add("collapsed");
        formToggle.addEventListener("click", function (e) {
          accordionForm.classList.toggle("collapsed");
          formToggle.classList.toggle("closed");
        });
      };
  }

  // Category list accordion
  const cats = document.getElementById("event-category-search-categories-with-children");
  if (cats) {
      catToggle = document.querySelector(".category-toggle");
      catToggle.addEventListener("click", function (e) {
          cats.classList.toggle("collapsed");
          catToggle.classList.toggle("closed");
        });
  }
  if (window.location.href.includes(encodeURI('/node/add/event?edit[field_organization][widget][form][entity_id]'))) {
    let selector = `fieldset[data-drupal-selector^="edit-field-organization-form"]`;
    let panel = document.querySelector(selector);
    let addOrgButton = panel.getElementsByTagName("button")[0];
    if (addOrgButton) {
      triggerMouseEvent(addOrgButton, "mousedown");
    }
  };

  if (window.location.href.includes(encodeURI('/calendar'))) {
    let selector = `.tabs.is-toggle a.is-active`;
    let tab = document.querySelector(selector);
    let parent = tab.parentElement;
    parent.classList.add('is-active');
  };

}); // end window load event

const orgTabs = () => {
  //Organization Tabs
  const orgTabSelector = document.getElementsByClassName("organization-tabs_tab");
  if (orgTabSelector) {
    const orgPanels = [];
    const orgTabs = Array.from(orgTabSelector);
    orgTabs.forEach(tab => {
      const panelID = tab.getElementsByTagName("a")[0].getAttribute('href', 2);
      const selector = `fieldset[data-drupal-selector^="${panelID}"]`;
      const panel = document.querySelector(selector);
      if (panel) {
        panel.classList.add('organization-panel');
        orgPanels.push(panel);
      }
      tab.addEventListener('click', e => {
        e.preventDefault();
        // if the create org tab is clicked, just mousedown on the button to trigger ajax form load
        if(panelID === "edit-field-organization-actions" && panel !== null ) {
          const createOrgButton = panel.getElementsByTagName("button")[0];
          if (createOrgButton) {
            triggerMouseEvent (createOrgButton, "mousedown");
          }
        } else {
          const tabID = e.target.getAttribute('href', 2);
          orgPanels.forEach(t => {
            t.classList.remove('is-active');
          } )
          if (panel !== null) {
            panel.classList.add('is-active');
          }
        }
      })
    });
  }
}

const triggerMouseEvent = (node, eventType) => {
  var clickEvent = document.createEvent ('MouseEvents');
  clickEvent.initEvent (eventType, true, true);
  node.dispatchEvent (clickEvent);
}

// orgTabs needs to be done whenever the form does ajax load
// to do: be more specific about the ajax call to react to
(function($){
  $(document).ready(function() {
    if (window.location.href.includes(encodeURI('/event'))) {
      orgTabs();
      let eventAddOrgButton = document.getElementsByClassName('ief-event-entity-submit')[0];
      let eventAddOrgTextInput = document.getElementsByClassName('event-form-add-org-input')[0];
      eventAddOrgButton.disabled = true;
      eventAddOrgTextInput.addEventListener('input', () => {
        eventAddOrgButton.disabled = eventAddOrgTextInput.value == '';
      })
    }
  });

  $(document).ajaxComplete(function(e, xhr, settings) {
    let eventAddOrgButton = document.getElementsByClassName('ief-event-entity-submit')[0];
    let eventAddOrgTextInput = document.getElementsByClassName('event-form-add-org-input')[0];
    if (eventAddOrgTextInput) {
      eventAddOrgButton.disabled = eventAddOrgTextInput.value == '';
      eventAddOrgTextInput.addEventListener('input', () => {
        eventAddOrgButton.disabled = eventAddOrgTextInput.value == '';
      })
    }
    if (((e.target.URL.indexOf("edit") >= 0 || (e.target.URL.indexOf("add") >= 0)  && e.target.URL.indexOf("event") >= 0))
      && (e.type == "ajaxComplete")) {
      orgTabs();
    }
  });
}(jQuery));


function searchIssue() {
  let input, filter, ul, li, categories, subCategories, issues;
  input = document.getElementById("issue-filter");
  filter = input.value.toUpperCase();
  categories = document.querySelectorAll(".category-list > li");
  details = document.querySelectorAll(".category-list details");

  if (filter != '') {
  details.forEach((detail) => {
      detail.open = true;
  });
  } else {
      details.forEach((detail) => {
          detail.open = false;
      });
  }

  for (let categoryKey in categories) {
      if (categories.hasOwnProperty(categoryKey)) {
          let category = categories[categoryKey]
          let categoryDisplay = false;
          let subCategories = category.querySelector(".view-id-subcategories_in_category .item-list").querySelectorAll(":scope > ul > li");
          for (let subCategoryKey in subCategories) {
              if (subCategories.hasOwnProperty(subCategoryKey)) {
                  let subCategory = subCategories[subCategoryKey];
                  let issues = subCategory.querySelectorAll(".view-id-issues_in_subcategory .item-list > ul > li");
                  var subCategoryDisplay = false;
                  for (let issueKey in issues) {
                      if (issues.hasOwnProperty(issueKey)) {
                          let issue = issues[issueKey];
                          let issueName = issue.querySelector(".issue-name").innerHTML;
                          if (issueName.toUpperCase().indexOf(filter) > -1) {
                              subCategoryDisplay = true;
                              categoryDisplay = true;
                              if (!issue.classList.contains("search-result")) {
                                issue.classList.toggle("search-result");
                              }
                              if (issue.classList.contains("not-search-result")) {
                                issue.classList.toggle("not-search-result");
                              }
                          } else {
                              if (issue.classList.contains("search-result")) {
                                issue.classList.toggle("search-result");
                              }
                              if (!issue.classList.contains("not-search-result")) {
                                issue.classList.toggle("not-search-result");
                              }
                          }
                      }
                  }
                  if (subCategoryDisplay != true) {
                      subCategory.style.display = "none";
                  } else {
                      subCategory.style.display = "block";
                  }
              }
          }
          if (categoryDisplay != true) {
              category.style.display = "none";
          } else {
              category.style.display = "block";
          }
      }
  }
}
